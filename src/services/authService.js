const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const {User} = require('../models/userModel');

const registration = async ({username, password}) => {
    const user = new User({
        username,
        password: await bcrypt.hash(password, 10)
    });
    await user.save();
}

const signIn = async ({username, password}) => {
    const user = await User.findOne({username});

    if (!user) {
        throw new Error('Invalid username or password');
    }

    if (!(await bcrypt.compare(password, user.password))) {
        throw new Error('Invalid username or password');
    }

    const token = jwt.sign({
        _id: user._id,
        username: user.username
    }, 'secret');
    return token;
}


const deleteUser = async (userId) => {
    await User.findOneAndRemove({_id: userId});
  };
  



const getUserByUserId = async (userId) => {
    const user = await User.find({userId});
    return user;
}


module.exports = {
    registration,
    signIn,
    deleteUser,
    getUserByUserId
};