const {Note} = require('../models/noteModel');

const getNotesByUserId = async (userId) => {
    const notes = await Note.find({userId});
    return notes;
}

const getNoteByIdForUser = async (noteId, userId) => {
    const note = await Note.findOne({_id: noteId, userId});
    return note;
}

const addNoteToUser = async (userId, notePayload) => {
    const note = new Note({...notePayload, userId});
    await note.save();
}


const deleteNoteByIdForUser = async (noteId, userId) => {
    await Note.findOneAndRemove({_id: noteId, userId});
}

const updateNoteByIdForUser = async (id, userId, dattexta) => {
    return await Note.findOneAndUpdate({_id: id, userId}, { $set: {text}});
 }
 

module.exports = {
    getNotesByUserId,
    getNoteByIdForUser,
    addNoteToUser,
    updateNoteByIdForUser,
    deleteNoteByIdForUser
};