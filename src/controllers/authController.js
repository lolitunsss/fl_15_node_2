const express = require('express');
const router = express.Router();

const {
    registration,
    signIn,
    deleteUser,
    getUserByUserId
} = require('../services/authService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    registrationValidator
} = require('../middlewares/validationMidlleware');

router.post('/register', registrationValidator, asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    await registration({username, password});

    res.json({message: 'Account created successfully!'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    const jwt_token = await signIn({username, password});

    res.json({jwt_token, message: 'Logged in successfully!'});
}));




router.delete('/me', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
  
    await deleteUser(userId);
  
    res.json({message: 'Success'});
  }));



router.get('/me', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const user = await getUserByUserId(userId);

    res.json({user});
}));




module.exports = {
    authRouter: router
}